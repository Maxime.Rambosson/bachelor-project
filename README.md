# STD Renderer
STD Renderer is a bachelor project with goal to provide a simple way to do rendering of scientific simulation without messing the performance.


# What is it

STD Renderer give the possibility to do image-rendering using C++17. This new standard provide native option to use the parallelism over standards algorithms. With specific compiler like `nvc++` compiler with `stdpar` option, this parallelism (and so the rendering) can occurs without any change on GPU, allowing to not move simulation data between CPU & GPU, saving performances.

# Installation

The simple way to install this project is to include the files of *src/* and *include/* currently. A cleaner way may be provided soon.  
Note : Include are done with `#include <header>` in project

# How to use it

## Create Shader
### How
The first step is to create an `Shader.hpp` file with `Shader`class, includable by `#include <Shader.hpp>` (we will explain why this file is not provided later).

At least this class should contain the method :  
`bool fragment(const std::array<Vec3f, 3>& model_coords, std::array<TGAColor, 3>& colors) const`  
The `colors` parameters is used to return the colors wanted for each vertex passed is `model_coords`  
The return value is used to discar a triangle, a `true` returned mean discar.

For the type used, you will also need :  
`#include <Geometry.hpp>`  
`struct TGAColor;`  

The rest is up to your need, in most case we will need to store a pointer to simulation data, used to calculate color.

### Why
We do not provide a general purpose shader to keep it customisable easly, as shown in `exemples/`.
Either, we don't use inheritance to create custom shader due to the lack of support of virtual classes in `nvc++`compiler with `stdpar`option, which was the main target in this project. If your compiler support it, then you can use it.

## Usage
Minimal exemple can be found [here](https://gitlab.unige.ch/Maxime.Rambosson/bachelor-project/-/blob/master/exemples/model/main.cpp)

The minimal include will be :
`#include <Renderer.hpp>`    
`#include <Image.hpp>`  
`#inlude <Shader.hpp>`  
`#inlude <Model.hpp>`  

First, create a Renderer :   
`Renderer renderer;`

Then create the model(s) you want to render :  
-  From OBJ file : `std::unique_ptr<Model> model{ Model::From_OBJ("head.obj") };`  
	(will need `#include <Model.hpp>`)
-  Using utils function :  
	`std::unique_ptr<Model> plane = create_plane(Vec3f{ -1.f, -1.f, 0.f }, Vec3f{ -1.f, 1.f, 0.f }, Vec3f{ 1.f, -1.f, 0.f }, 3);`  
	`std::unique_ptr<Model> sphere = create_sphere(Vec3f{ 0.f, 0.f, 0.f }, 0.5f, 20);`  
	(will need `#include <Utils.hpp>`)

Add it to the renderer :  
`renderer.add_model(std::move(plane));`  

Create an image and a shader :  
`std::unique_ptr<Shader> shader{ std::make_unique<Shader>() }; `  
`std::unique_ptr<TGAImage> image{ std::make_unique<TGAImage>(800, 800, TGAImage::RGB) };`  

Then render & save (in correct orientation) :   
`renderer.depth_render(image, shader);`  
`image->flip_vertically();`  
`image->write_tga_file("plane.tga");`  

# Documentation
## Renderer.hpp
### `size_t Renderer::add_model(std::unique_ptr<Model>&& model);`
Permit to add model to renderer without copy
Return the index of the added model

### `void Renderer::set_view_matrix(const Matrix4f& view);`

Permit to set the view matrix used to render
For this, use `Matrix4<T>` static function and utils functions from Geometry.hpp

### `void Renderer::render(std::unique_ptr<TGAImage>& surface, const std::unique_ptr<Shader>& shader) const;`

Simply render models on `surface`using `shader`. Do not use depth buffer, and if multiple triangles impact 1 pixel, only the last one rendered appear. Fastest render.

### `void Renderer::blend_render(std::unique_ptr<TGAImage>& surface, const std::unique_ptr<Shader>& shader) const;`

Take in count all triangles of all models impacting each pixel with `shader`, and make a blend of them before rendering on `surface`. 

### `void Renderer::depth_render(std::unique_ptr<TGAImage>& surface, const std::unique_ptr<Shader>& shader) const;`
Use depth buffer to render models on `surface`using `shader`. 

## Model.hpp
### `static std::unique_ptr<Model> Model::From_OBJ(const std::string& filename);`
Create a model from OBJ file

## Utils.hpp
### `std::unique_ptr<Model> create_plane(const Vec3f& A, const Vec3f& B, const Vec3f& C, unsigned nb_division);`

Create a plane from 3 coordinates (`A`, `B`, `C`). This plane is divided in (`nb_division` + 1)^2 squares, giving  (`nb_division` + 1)^2 * 2 triangles.

### `std::unique_ptr<Model> create_sphere(const Vec3f& center, float radius, unsigned nb_division);`

Create an UV sphere of center `center` and radius `radius` with `nb_division` segments and `nb_division`rings.

## Image.hpp

### `TGAImage::TGAImage(short w, short h, short bpp);`

Create an image with size (`w`, `h`) and `bpp` foramt.

### `bool TGAImage::flip_horizontally();`

Flip the current image horizontally.
	
### `bool TGAImage::flip_vertically();`

Flip the current image vertically.

### `bool TGAImage::write_tga_file(const char *filename, bool rle=true);`

Write the current image on disk to `filename`. If `rle` is true, the it use RLE compression.

### `enum TGAImage::Format`

Helper enum to fill `bpp` parameter of `TGAImage` constructor.
Values : `GRAYSCALE=1, RGB=3, RGBA=4`

## Geometry.hpp
### `Vec3::Vec3(T x, T y, T z);`

Create a Vector3 of type `T` with coordinate (`x`, `y`, `z`).

### `static Matrix4 Matrix4::Translation(const Vec3<T>& translation);`

Create a translation matrix by `translation` vector.

### `static Matrix4 Matrix4::Rotation(float angle, Vec3<T> axis);`

Create a rotation matrix by `angle` folowing `axis`.

### `float to_radiant(float degree);`

Convert `degree`to radiant.
