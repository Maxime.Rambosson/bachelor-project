// *****************************************************************************
// STD Renderer

// Authors: Maxime Rambosson
// Contact: Maxime.Rambosson@etu.unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#include <vector>
#include <string>
#include <chrono>
#include <algorithm>

#include <Geometry.hpp>
#include <Image.hpp>
#include <Model.hpp>
#include <Renderer.hpp>
#include <Utils.hpp>
#include <Shader.hpp>

std::vector<unsigned> init_dummy_data(unsigned nb_axis_point) {
	std::vector<unsigned> data(nb_axis_point * nb_axis_point * nb_axis_point, 0);

	for (size_t z = 0; z < nb_axis_point; ++z) {
		unsigned dz = std::abs(int(z - nb_axis_point / 2));
		unsigned z_distance = dz * dz;
		for (size_t y = 0; y < nb_axis_point; ++y) {
			unsigned dy = std::abs(int(y - nb_axis_point / 2));
			unsigned y_distance = dy * dy;
			for (size_t x = 0; x < nb_axis_point; ++x) {
				unsigned dx = std::abs(int(x - nb_axis_point / 2));
				data[z * nb_axis_point * nb_axis_point + y * nb_axis_point + x] = z_distance + y_distance + dx * dx;
			}
		}
	}

	return data;
}

void print_3D_matrix(const std::vector<unsigned>& data, unsigned nb_axis_point) {
	for (size_t z = 0; z < nb_axis_point; ++z) {
		std::cout << "\n";
		for (size_t y = 0; y < nb_axis_point; ++y) {
			std::cout << "\n";
			for (size_t x = 0; x < nb_axis_point; ++x) {
				std::cout << data[z * nb_axis_point * nb_axis_point + y * nb_axis_point + x] << " ";
			}
		}
	}
	std::cout << "\n";
}

int main(){
	constexpr unsigned simulation_dimension = 500;

	std::vector<unsigned> dummy_simulation{ init_dummy_data(simulation_dimension) };

	Renderer renderer;
	renderer.set_view_matrix( Matrix4f::Translation(Vec3f{ 0.f, 0.f, -0.5f }) );
	auto start_generaton_time{ std::chrono::high_resolution_clock::now() };
	std::unique_ptr<Model> model{ create_plane(Vec3f{ -1.f, -1.f, 0.4f }, Vec3f{ 1.f, -1.f, 0.4f }, Vec3f{ -1.f, 1.f, 0.4f }, simulation_dimension) };
	auto end_generation_time{ std::chrono::high_resolution_clock::now() };
	renderer.add_model(std::move(model));

	std::chrono::duration<double, std::milli> complete_generation_time{ end_generation_time - start_generaton_time };
	std::cout << "Generation : " << complete_generation_time.count() / 1000 << std::endl;

	unsigned data_max = *std::max_element(dummy_simulation.begin(), dummy_simulation.end());
	const unsigned* data_ptr = dummy_simulation.data();

	std::unique_ptr<Shader> shader{ std::make_unique<Shader>(data_ptr, data_max, simulation_dimension, simulation_dimension, simulation_dimension) };

	auto start_render_time{ std::chrono::high_resolution_clock::now() };
	for (unsigned i = 0; i < 10; ++i) {		
		std::unique_ptr<TGAImage> image{ std::make_unique<TGAImage>(3200, 3200, TGAImage::RGB) };

		renderer.render(image, shader);

		image->flip_vertically();
		image->write_tga_file(std::string("output" + std::to_string(i) + ".tga").c_str());
	}
	auto end_render_time{ std::chrono::high_resolution_clock::now() };

	std::chrono::duration<double, std::milli> complete_render_time{ end_render_time - start_render_time };
	std::cout << "Render : " << complete_render_time.count() / 1000 << std::endl;

	return 0;
}
