// *****************************************************************************
// STD Renderer

// Authors: Maxime Rambosson
// Contact: Maxime.Rambosson@etu.unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#include <Shader.hpp>

#include <Image.hpp>

inline bool Shader::fragment(const std::array<Vec3f, 3>& model_coords, std::array<TGAColor, 3>& colors) const {
	Vec3f light_dir{ 0, 0, -1 };
	Vec3f n = (model_coords[2] - model_coords[0]) ^ (model_coords[1] - model_coords[0]);
	n.normalize();

	float intensity = n * light_dir;
	if (intensity < 0) {
		return true;
	}

	for (size_t i = 0; i < 3; ++i) {
		colors[i] = TGAColor{ (unsigned char)(intensity * 255), (unsigned char)(intensity * 255), (unsigned char)(intensity * 255), 255 };
	}

	return false;
}

