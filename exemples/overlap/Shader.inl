// *****************************************************************************
// STD Renderer

// Authors: Maxime Rambosson
// Contact: Maxime.Rambosson@etu.unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#include <Shader.hpp>

#include <Image.hpp>

inline bool Shader::fragment(const std::array<Vec3f, 3>& model_coords, std::array<TGAColor, 3>& colors) const {

	for (size_t i = 0; i < 3; ++i) {
		const Vec3f& coords{ (model_coords[i] + 1) / 2 };
		size_t z = size_t(std::round((m_dim_z - 1) * coords.z));
		size_t y = size_t(std::round((m_dim_y - 1) * coords.y));
		size_t x = size_t(std::round((m_dim_x - 1) * coords.x));

		size_t simulation_index = z * m_dim_z * m_dim_z + y * m_dim_x + x;

		colors[i] = m_data_ptr[simulation_index];
	}

	return false;
}

