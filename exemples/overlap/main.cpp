// *****************************************************************************
// STD Renderer

// Authors: Maxime Rambosson
// Contact: Maxime.Rambosson@etu.unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#include <vector>
#include <string>
#include <chrono>
#include <random>

#include <Geometry.hpp>
#include <Image.hpp>
#include <Model.hpp>
#include <Renderer.hpp>
#include <Utils.hpp>
#include <Shader.hpp>

std::vector<TGAColor> init_dummy_color_data(unsigned nb_axis_point) {
	std::vector<TGAColor> data(nb_axis_point * nb_axis_point * nb_axis_point);

	for (size_t z = 0; z < nb_axis_point; ++z) {
		size_t z_offset = z * nb_axis_point * nb_axis_point;
		for (size_t y = 0; y < nb_axis_point; ++y) {
			size_t y_offset = y * nb_axis_point;
			for (size_t x = 0; x < nb_axis_point; ++x) {
				if (x % 3 == 0) {
					data[z_offset + y_offset + x] = TGAColor{ 255, 0, 0, 255 };
				}
				else if (x % 3 == 1) {
					data[z_offset + y_offset + x] = TGAColor{ 0, 255, 0, 255 };
				}
				else {
					data[z_offset + y_offset + x] = TGAColor{ 0, 0, 255, 255 };
				}
				
			}
		}
	}

	return data;
}

template<typename T>
void print_3D_matrix(const std::vector<T>& data, unsigned nb_axis_point) {
	for (size_t z = 0; z < nb_axis_point; ++z) {
		std::cout << "\n";
		for (size_t y = 0; y < nb_axis_point; ++y) {
			std::cout << "\n";
			for (size_t x = 0; x < nb_axis_point; ++x) {
				std::cout << data[z * nb_axis_point * nb_axis_point + y * nb_axis_point + x] << " ";
			}
		}
	}
	std::cout << "\n";
}

int main(){
	constexpr unsigned simulation_dimension = 8;

	std::vector<TGAColor> dummy_data{ init_dummy_color_data(simulation_dimension) };

	Renderer renderer;
	//renderer.set_view_matrix(Matrix4f::Translation(Vec3f{ 0.f, 0.f, -2.f }));
	renderer.set_perspective_projection(true);

	std::unique_ptr<Model> model{ create_plane(Vec3f{ -0.9f, -0.9f, 0.0f }, Vec3f{ -0.9f, 0.9f, 0.0f }, Vec3f{ 0.9f, -0.9f, 0.0f }, simulation_dimension) };
	renderer.add_model(std::move(model));

	std::unique_ptr<Shader> shader{ std::make_unique<Shader>(dummy_data.data(), simulation_dimension, simulation_dimension, simulation_dimension) };

	std::unique_ptr<TGAImage> original_image{ std::make_unique<TGAImage>(simulation_dimension, simulation_dimension, TGAImage::RGB) };
	renderer.render(original_image, shader);
	original_image->flip_vertically();
	original_image->write_tga_file("original.tga");


	std::unique_ptr<TGAImage> image{ std::make_unique<TGAImage>(simulation_dimension - 2, simulation_dimension - 2, TGAImage::RGB) };
	renderer.render(image, shader);
	image->flip_vertically();
	image->write_tga_file("basic_overlap.tga");

	std::unique_ptr<TGAImage> blended_image{ std::make_unique<TGAImage>(simulation_dimension - 2, simulation_dimension - 2, TGAImage::RGB) };
	renderer.blend_render(blended_image, shader);
	blended_image->flip_vertically();
	blended_image->write_tga_file("blended_overlap.tga");

	std::cout << "Render done" << std::endl;

	return 0;
}
