// *****************************************************************************
// STD Renderer

// Authors: Maxime Rambosson
// Contact: Maxime.Rambosson@etu.unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#include <Shader.hpp>

#include <Image.hpp>

#include <tuple>
#include <algorithm>

inline double linear(double x, double x1, double x2, double y1, double y2) {
    return ((y2 - y1) * x + x2 * y1 - x1 * y2) / (x2 - x1);
}

// For good visibility, use a color scheme that goes from black-blue to black-red.
inline auto colorScheme(double value) {
 
    double r = 0., g = 0., b = 0.;
    if (value <= 1. / 8.) {
        r = 0.;
        g = 0.;
        b = linear(value, -1. / 8., 1. / 8., 0., 1.);
    } else if (value <= 3. / 8.) {
        r = 0.;
        g = linear(value, 1. / 8., 3. / 8., 0., 1.);
        b = 1.;
    } else if (value <= 5. / 8.) {
        r = linear(value, 3. / 8., 5. / 8., 0., 1.);
        g = 1.;
        b = linear(value, 3. / 8., 5. / 8., 1., 0.);
    } else if (value <= 7. / 8.) {
        r = 1.;
        g = linear(value, 5. / 8., 7. / 8., 1., 0.);
        b = 0.;
    } else {
        r = linear(value, 7. / 8., 9. / 8., 1., 0.);
        g = 0.;
        b = 0.;
    }
 
    r = std::min(255. * r, 255.);
    g = std::min(255. * g, 255.);
    b = std::min(255. * b, 255.);
    
    return TGAColor{ (unsigned char)(r), (unsigned char)(g), (unsigned char)(b), 255 };
}

inline bool Shader::fragment(const std::array<Vec3f, 3>& model_coords, std::array<TGAColor, 3>& colors) const {
	const double max_value =  0.01f; //*std::max_element(m_data_ptr, m_data_ptr + (m_dim_z * m_dim_y * m_dim_x));

	for (size_t i = 0; i < 3; ++i) {
		const Vec3f& coords{ model_coords[i] };
		size_t z = size_t(std::round((m_dim_z - 1) * coords.z));
		size_t y = size_t(std::round((m_dim_y - 1) * coords.y));
		size_t x = size_t(std::round((m_dim_x - 1) * coords.x));

		size_t simulation_index = z * m_dim_z * m_dim_z + y * m_dim_y + x;
		double value = m_data_ptr[simulation_index];

		colors[i] = colorScheme(value / max_value);
	}



	return false;
}
 
