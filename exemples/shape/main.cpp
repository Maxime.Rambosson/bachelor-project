// *****************************************************************************
// STD Renderer

// Authors: Maxime Rambosson
// Contact: Maxime.Rambosson@etu.unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#include <Geometry.hpp>
#include <Image.hpp>
#include <Model.hpp>
#include <Renderer.hpp>
#include <Shader.hpp>
#include <Utils.hpp>

int main() {
	Renderer renderer;
	renderer.set_view_matrix(Matrix4f::Translation(Vec3f{ 0.f, 0.f, -0.5f }));

	std::unique_ptr<Model> plane1 = create_plane(Vec3f{ -1.f, -1.f, 0.f }, Vec3f{ -1.f, 1.f, 0.f }, Vec3f{ 1.f, -1.f, 0.f }, 3);
	renderer.add_model(std::move(plane1));

	std::unique_ptr<Model> plane2 = create_plane(Vec3f{ 0.5f, -1.f, -1.f }, Vec3f{ 0.5f, -1.f, 1.f }, Vec3f{ 1.f, 1.f, -1.f }, 3);
	renderer.add_model(std::move(plane2));
	
	std::unique_ptr<Model> plane3 = create_plane(Vec3f{ -1.f, -0.5f, -1.f }, Vec3f{ -1.f, -0.5f, 1.f }, Vec3f{ 1.f, -0.5f, -1.f }, 3);
	renderer.add_model(std::move(plane3));
	
	std::unique_ptr<Model> sphere = create_sphere(Vec3f{ 0.f, 0.f, 0.f }, 0.5f, 20);
	renderer.add_model(std::move(sphere));

	std::unique_ptr<Shader> shader{ std::make_unique<Shader>() };

	std::unique_ptr<TGAImage> image{ std::make_unique<TGAImage>(800, 800, TGAImage::RGB) };
	renderer.depth_render(image, shader);

	image->flip_vertically();
	image->write_tga_file("plane.tga");

	return 0;
}

