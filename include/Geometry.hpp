// *****************************************************************************
// STD Renderer

// Authors: Maxime Rambosson
// Contact: Maxime.Rambosson@etu.unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#ifndef GEOMETRY_HPP
#define GEOMETRY_HPP

#include <cmath>
#include <iostream>
#include <cassert>
#include <array>

template<typename T> class Matrix4;

template <typename T>
struct Vec2 {
	T x, y;

	Vec2();
	Vec2(T x, T y);
	template<typename U> Vec2<T>(const Vec2<U>& v);

	inline Vec2<T> operator+(const Vec2<T>& V) const;
	inline Vec2<T> operator-(const Vec2<T>& V) const;
	inline Vec2<T> operator*(T scalar) const;
	inline Vec2<T> operator/(T scalar) const;
	inline bool operator==(const Vec2<T>& v) const;

	inline T& operator[](size_t i);
	inline const T& operator[](size_t i) const;
};

using Vec2f = Vec2<float>;
using Vec2i = Vec2<int>;

template <class T> 
std::ostream& operator<<(std::ostream& s, const Vec2<T>& v);

template <class T>
struct Vec4;

template <class T>
struct Vec3 {
	T x, y, z;

	Vec3();
	Vec3(T x, T y, T z);
	template<typename U> Vec3<T>(const Vec3<U>& v);

	inline Vec3<T> operator^(const Vec3<T>& v) const;
	inline Vec3<T> operator+(const Vec3<T>& v) const;
	inline Vec3<T> operator+(T scalar) const;
	inline Vec3<T> operator-(const Vec3<T>& v) const;
	inline Vec3<T> operator-(T scalar) const;
	inline Vec3<T> operator*(T scalar) const;
	inline Vec3<T> operator/(T scalar) const;
	inline T operator*(const Vec3<T>& v) const;
	inline bool operator==(const Vec3<T>& v) const;
	inline bool operator<(const Vec3<T>& v) const;

	inline T& operator[](size_t i);
	inline const T& operator[](size_t i) const;

	float squared_length() const;
	float length() const;
	Vec3<T>& normalize();

	static Vec3<T> FromMatrix(const Matrix4<T>& mat);
	static Vec3<T> FromVec4(const Vec4<T>& vec);
};

using Vec3f = Vec3<float>;
using Vec3i = Vec3<int>;

template<typename T>
Vec3<T> operator*(const Vec3<T>& vec, T scalar);

template<typename T>
Vec3<T> operator*(T scalar, const Vec3<T>& vec);

template<class T>
Vec3<T> cross(const Vec3<T>& v0, const Vec3<T>& v1);

inline Vec3f barycentric(const Vec3f& A, const Vec3f& B, const Vec3f& C, const Vec3f& P);

inline float triangle_area(const Vec3i& p1, const Vec3i& p2, const Vec3i& p3);

template <class T>
std::ostream& operator<<(std::ostream& s, const Vec3<T>& v);

template <class T>
struct Vec4 {
	T x, y, z, w;

	Vec4();
	Vec4(T x, T y, T z, T w);
	Vec4(const Vec3<T>& v, T w);
	template<typename U> Vec4<T>(const Vec4<U>& v);

	inline Vec4<T> operator+(const Vec4<T>& v) const;
	inline Vec4<T> operator-(const Vec4<T>& v) const;
	inline Vec4<T> operator/(T scalar) const;
	inline bool operator==(const Vec4<T>& v) const;

	inline T& operator[](size_t i);
	inline const T& operator[](size_t i) const;
};

using Vec4f = Vec4<float>;
using Vec4i = Vec4<int>;

template<typename T>
Vec4<T> operator*(const Vec4<T>& vec, T scalar);

template<typename T>
Vec4<T> operator*(T scalar, const Vec4<T>& vec);

template <class T>
std::ostream& operator<<(std::ostream& s, const Vec4<T>& v);

template<typename T>
class Matrix3 {
public:
	Matrix3() = default;
	Matrix3(T e00, T e01, T e02,
			T e10, T e11, T e12,
			T e20, T e21, T e22);
	~Matrix3() = default;

	inline std::array<T, 3>& operator[](size_t i);
	inline const std::array<T, 3>& operator[](size_t i) const;

	bool operator==(const Matrix3& mat) const;

	float det() const;

	static Matrix3 Identity();
private:
	std::array<std::array<T, 3>, 3> m_data;
};

using Matrix3i = Matrix3<int>;
using Matrix3f = Matrix3<float>;

template<typename T>
std::ostream& operator<<(std::ostream& out, const Matrix3<T>& mat);

// ROW MAJOR
template<typename T>
class Matrix4 {
public:
	Matrix4() = default;
	Matrix4(T e00, T e01, T e02, T e03,
			T e10, T e11, T e12, T e13, 
			T e20, T e21, T e22, T e23, 
			T e30, T e31, T e32, T e33 );
	~Matrix4() = default;

	inline std::array<T, 4>& operator[](size_t i); 
	inline const std::array<T, 4>& operator[](size_t i) const;
	
	Matrix4 operator*(const Matrix4& mat) const;
	Matrix4 operator+(const Matrix4& mat) const;
	Matrix4 operator-(const Matrix4& mat) const;
	bool operator==(const Matrix4& mat) const;

	Matrix3<T> toMatrix3(size_t row, size_t column) const;

	float det() const;
	Matrix4 adj() const;
	Matrix4 inv() const;

	static Matrix4 Identity();
	static Matrix4 Perspective(float fov_angle, float aspect_ratio, float near_clipping, float far_clipping);
	static Matrix4 Translation(const Vec3<T>& translation);
	static Matrix4 Rotation(float angle, Vec3<T> axis);
	static Matrix4 Viewport(int x, int y, unsigned w, unsigned h, unsigned depth);
	static Matrix4 FromVec(const Vec3<T>& vec);

private:
	std::array<std::array<T, 4>, 4> m_data;
};

using Matrix4i = Matrix4<int>;
using Matrix4f = Matrix4<float>;

template<typename T>
Matrix4<T> operator*(const Matrix4<T>& mat, T scalar);

template<typename T>
Matrix4<T> operator*(T scalar, const Matrix4<T>& mat);

template<typename T>
Vec4<T> operator*(const Matrix4<T>& mat, const Vec4<T>& vec);

template<typename T>
std::ostream& operator<<(std::ostream& out, const Matrix4<T>& mat);

float to_radiant(float degree);

#include <Geometry.inl>

#endif
