// *****************************************************************************
// STD Renderer

// Authors: Maxime Rambosson
// Contact: Maxime.Rambosson@etu.unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#include <Geometry.hpp>

#include <cmath>
#include <iostream>
#include <cassert>

template <typename T>
Vec2<T>::Vec2() : x(0), y(0) { }

template <typename T>
Vec2<T>::Vec2(T x, T y) : x(x), y(y) { }

template <typename T>
template <typename U>
Vec2<T>::Vec2(const Vec2<U>& v) : x(T(v.x)), y(T(v.y)) { }

template <typename T>
inline Vec2<T> Vec2<T>::operator+(const Vec2<T>& v) const {
	return Vec2<T>{ x + v.x, y + v.y };
}

template <typename T>
inline Vec2<T> Vec2<T>::operator-(const Vec2<T>& v) const {
	return Vec2<T>{ x - v.x, y - v.y };
}

template <typename T>
inline Vec2<T> Vec2<T>::operator*(T scalar) const {
	return Vec2<T>{ x* scalar, y* scalar };
}

template<typename T>
inline Vec2<T> Vec2<T>::operator/(T scalar) const {
	return Vec2<T>{ x / scalar, y / scalar };
}

template<typename T>
inline bool Vec2<T>::operator==(const Vec2<T>& v) const {
	return x == v.x && y == v.y;
}

template <typename T>
inline T& Vec2<T>::operator[](size_t i) {
	assert(i < 2);
	return i ? y : x;
}

template <typename T>
inline const T& Vec2<T>::operator[](size_t i) const {
	assert(i < 2);
	return i ? y : x;
}

template <class T>
std::ostream& operator<<(std::ostream& s, const Vec2<T>& v) {
	s << "(" << v.x << ", " << v.y << ")";
	return s;
}

template <class T>
Vec3<T>::Vec3() : x(0), y(0), z(0) { }

template <class T>
Vec3<T>::Vec3(T x, T y, T z) : x(x), y(y), z(z) { }

template <class T>
template <class U>
Vec3<T>::Vec3(const Vec3<U>& v) : x(T(v.x)), y(T(v.y)), z(T(v.z)) { }

template <class T>
inline Vec3<T> Vec3<T>::operator^(const Vec3<T>& v) const {
	return Vec3<T>{ y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x };
}

template <class T>
inline Vec3<T> Vec3<T>::operator+(const Vec3<T>& v) const {
	return Vec3<T>{ x + v.x, y + v.y, z + v.z };
}

template<class T>
inline Vec3<T> Vec3<T>::operator+(T scalar) const {
	return Vec3<T>{ x + scalar, y + scalar, z + scalar };
}

template <class T>
inline Vec3<T> Vec3<T>::operator-(const Vec3<T>& v) const {
	return Vec3<T>{ x - v.x, y - v.y, z - v.z };
}

template<class T>
inline Vec3<T> Vec3<T>::operator-(T scalar) const {
	return Vec3<T>{ x - scalar, y - scalar, z - scalar };
}

template <class T>
inline Vec3<T> Vec3<T>::operator*(T scalar) const {
	return Vec3<T>{ x * scalar, y * scalar, z * scalar };
}

template<class T>
inline Vec3<T> Vec3<T>::operator/(T scalar) const {
	return Vec3<T>{ x / scalar, y / scalar, z / scalar };
}

template <class T>
inline T Vec3<T>::operator*(const Vec3<T>& v) const {
	return x * v.x + y * v.y + z * v.z;
}

template<class T>
inline bool Vec3<T>::operator==(const Vec3<T>& v) const {
	return x == v.x && y == v.y && z == v.z;
}

template<class T>
inline bool Vec3<T>::operator<(const Vec3<T>& v) const {
	return x < v.x || (x == v.x && y < v.y) || (x == v.x && y == v.y && z < v.z);
}

template <class T>
inline T& Vec3<T>::operator[](size_t i) { 
	assert(i < 3); 
	return i ? (i == 1 ? y : z) : x; 
}

template <class T>
inline const T& Vec3<T>::operator[](size_t i) const {
	assert(i < 3);
	return i ? (i == 1 ? y : z) : x;
}

template<class T>
inline float Vec3<T>::squared_length() const {
	return x * x + y * y + z * z;
}

template <class T>
float Vec3<T>::length() const {
	return std::sqrt(x * x + y * y + z * z);
}

template <class T>
Vec3<T>& Vec3<T>::normalize() {
	*this = (*this) * (1 / length());
	return *this;
}

template <class T>
Vec3<T> Vec3<T>::FromMatrix(const Matrix4<T>& mat) {
	return Vec3<T>{ mat[0][0] / mat[3][0], mat[1][0] / mat[3][0], mat[2][0] / mat[3][0] };
}

template<class T>
inline Vec3<T> Vec3<T>::FromVec4(const Vec4<T>& vec) {
	return Vec3<T>{ vec.x, vec.y, vec.z };
}

template<typename T>
inline Vec3<T> operator*(const Vec3<T>& vec, T scalar) {
	return Vec3<T>{ vec.x * scalar, vec.y * scalar, vec.z * scalar };
}

template<typename T>
inline Vec3<T> operator*(T scalar, const Vec3<T>& vec) {
	return vec * scalar;
}

template<typename T>
Vec3<T> cross(const Vec3<T>& v0, const Vec3<T>& v1) {
	const T x = v0.y * v1.z - v0.z * v1.y;
	const T y = v0.z * v1.x - v0.x * v1.z;
	const T z = v0.x * v1.y - v0.y * v1.x;

	return Vec3<T>{ x, y, z };
}

inline Vec3f barycentric(const Vec3f& A, const Vec3f& B, const Vec3f& C, const Vec3f& P) {
	if (A == B && A == C) {
		return A;
	}
	
	Vec3f s[2]; // std::array
	s[0].x = C.x - A.x;
	s[0].y = B.x - A.x;
	s[0].z = A.x - P.x;

	s[1].x = C.y - A.y;
	s[1].y = B.y - A.y;
	s[1].z = A.y - P.y;

	Vec3f u{ cross(s[0], s[1]) };
	if (std::abs(u.z) > 1e-2) // dont forget that u[2] is integer. If it is zero then triangle ABC is degenerate
		return Vec3f{ 1.f - (u.x + u.y) / u.z, u.y / u.z, u.x / u.z };
	return Vec3f{ -1, 1, 1 }; // in this case generate negative coordinates, it will be thrown away by the rasterizator
}

inline float triangle_area(const Vec3i& p1, const Vec3i& p2, const Vec3i& p3) {
	return 0.5f * std::abs((p1.x - p3.x) * (p2.y - p1.y) - (p1.x - p2.x) * (p3.y - p1.y));
}

inline float to_radiant(float degree) {
	constexpr float pi = 3.14159265358979323846f;
	return degree * pi/180;
}

template<class T>
inline Vec4<T>::Vec4() : x(0), y(0), z(0), w(0) { }

template<class T>
inline Vec4<T>::Vec4(T x, T y, T z, T w) : x(x), y(y), z(z), w(w) { }

template<class T>
inline Vec4<T>::Vec4(const Vec3<T>& v, T w) : x(v.x), y(v.y), z(v.z), w(w) { }

template<class T>
inline Vec4<T> Vec4<T>::operator+(const Vec4<T>& v) const {
	return Vec4<T>{ x + v.x, y + v.y, z + v.z, w + v.w };
}

template<class T>
inline Vec4<T> Vec4<T>::operator-(const Vec4<T>& v) const {
	return Vec4<T>{ x + v.x, y + v.y, z + v.z, w + v.w };
}

template<class T>
inline Vec4<T> Vec4<T>::operator/(T scalar) const {
	return Vec4<T>{ x / scalar, y / scalar, z / scalar, w / scalar };
}

template<class T>
inline bool Vec4<T>::operator==(const Vec4<T>& v) const {
	return v.x == x && v.y == y && v.z == z && v.w == w;
}

template<class T>
inline T& Vec4<T>::operator[](size_t i) {
	assert(i < 4);
	return i ? (i == 1 ? y : (i == 2 ? z : w)) : x;
}

template<class T>
inline const T& Vec4<T>::operator[](size_t i) const {
	assert(i < 4);
	return i ? (i == 1 ? y : (i == 2 ? z : w)) : x;
}

template<class T>
template<typename U>
inline Vec4<T>::Vec4(const Vec4<U>& v) : x(T(v.x)), y(T(v.y)), z(T(v.z)), w(T(v.w)) { }



template <class T> 
std::ostream& operator<<(std::ostream& s, const Vec3<T>& v) {
	s << "(" << v.x << ", " << v.y << ", " << v.z << ")";
	return s;
}

template<typename T>
inline Vec4<T> operator*(const Vec4<T>& vec, T scalar) {
	return Vec4<T>{ vec.x * scalar, vec.y * scalar, vec.z * scalar, vec.w * scalar };
}

template<typename T>
inline Vec4<T> operator*(T scalar, const Vec4<T>& vec) {
	return vec * scalar;
}

template<class T>
inline std::ostream& operator<<(std::ostream& s, const Vec4<T>& v) {
	s << "(" << v.x << ", " << v.y << ", " << v.z << ", " << v.w << ")";
	return s;
}

template<typename T>
inline Matrix3<T>::Matrix3(T e00, T e01, T e02,
						   T e10, T e11, T e12,
						   T e20, T e21, T e22) {
	m_data[0] = std::array<T, 3>{ e00, e01, e02 };
	m_data[1] = std::array<T, 3>{ e10, e11, e12 };
	m_data[2] = std::array<T, 3>{ e20, e21, e22 };
}

template<typename T>
inline std::array<T, 3>& Matrix3<T>::operator[](size_t i) {
	return m_data[i];
}

template<typename T>
inline const std::array<T, 3>& Matrix3<T>::operator[](size_t i) const {
	return m_data[i];
}

template<typename T>
inline bool Matrix3<T>::operator==(const Matrix3& mat) const {
	return m_data[0] == mat[0] && m_data[1] == mat[1] && m_data[2] == mat[2];
}

template<typename T>
inline float Matrix3<T>::det() const {
	return m_data[0][0] * m_data[1][1] * m_data[2][2] + m_data[0][2] * m_data[1][0] * m_data[2][1] + m_data[0][1] * m_data[1][2] * m_data[2][0]
		- (m_data[0][2] * m_data[1][1] * m_data[2][0]) - (m_data[0][1] * m_data[1][0] * m_data[2][2]) - (m_data[0][0] * m_data[1][2] * m_data[2][1]);
}

template<typename T>
inline Matrix3<T> Matrix3<T>::Identity() {
	return Matrix3<T>{ T(1.f), T(0.f), T(0.f),
					   T(0.f), T(1.f), T(0.f),
					   T(0.f), T(0.f), T(1.f) };
}

template<typename T>
inline std::ostream& operator<<(std::ostream& out, const Matrix3<T>& mat) {
	out << "Matrix3(";
	for (size_t i{ 0 }; i < 3; ++i) {
		for (size_t j{ 0 }; j < 3; ++j) {
			out << mat[i][j] << ", ";
		}

		out << "; ";
	}

	out << ")";

	return out;
}

template<typename T>
inline Matrix4<T>::Matrix4(T e00, T e01, T e02, T e03,
						   T e10, T e11, T e12, T e13,
						   T e20, T e21, T e22, T e23,
						   T e30, T e31, T e32, T e33) {
	m_data[0] = std::array<T, 4>{ e00, e01, e02, e03 };
	m_data[1] = std::array<T, 4>{ e10, e11, e12, e13 };
	m_data[2] = std::array<T, 4>{ e20, e21, e22, e23 };
	m_data[3] = std::array<T, 4>{ e30, e31, e32, e33 };
}

template<typename T>
inline std::array<T, 4>& Matrix4<T>::operator[](size_t i) {
	return m_data[i];
}

template<typename T>
inline const std::array<T, 4>& Matrix4<T>::operator[](size_t i) const {
	return m_data[i];
}

template<typename T>
inline Matrix4<T> Matrix4<T>::operator*(const Matrix4<T>& mat) const {
	Matrix4<T> result;

	const std::array<T, 4>& data_0 = m_data[0];
	const std::array<T, 4>& data_1 = m_data[1];
	const std::array<T, 4>& data_2 = m_data[2];
	const std::array<T, 4>& data_3 = m_data[3];

	const std::array<T, 4>& mat_0 = mat[0];
	const std::array<T, 4>& mat_1 = mat[1];
	const std::array<T, 4>& mat_2 = mat[2];
	const std::array<T, 4>& mat_3 = mat[3];

	result[0] = std::array<T, 4>{
		data_0[0] * mat_0[0] + data_0[1] * mat_1[0] + data_0[2] * mat_2[0] + data_0[3] * mat_3[0],
		data_0[0] * mat_0[1] + data_0[1] * mat_1[1] + data_0[2] * mat_2[1] + data_0[3] * mat_3[1],
		data_0[0] * mat_0[2] + data_0[1] * mat_1[2] + data_0[2] * mat_2[2] + data_0[3] * mat_3[2],
		data_0[0] * mat_0[3] + data_0[1] * mat_1[3] + data_0[2] * mat_2[3] + data_0[3] * mat_3[3]
	};

	result[1] = std::array<T, 4>{
		data_1[0] * mat_0[0] + data_1[1] * mat_1[0] + data_1[2] * mat_2[0] + data_1[3] * mat_3[0],
		data_1[0] * mat_0[1] + data_1[1] * mat_1[1] + data_1[2] * mat_2[1] + data_1[3] * mat_3[1],
		data_1[0] * mat_0[2] + data_1[1] * mat_1[2] + data_1[2] * mat_2[2] + data_1[3] * mat_3[2],
		data_1[0] * mat_0[3] + data_1[1] * mat_1[3] + data_1[2] * mat_2[3] + data_1[3] * mat_3[3]
	};

	result[2] = std::array<T, 4>{
		data_2[0] * mat_0[0] + data_2[1] * mat_1[0] + data_2[2] * mat_2[0] + data_2[3] * mat_3[0],
		data_2[0] * mat_0[1] + data_2[1] * mat_1[1] + data_2[2] * mat_2[1] + data_2[3] * mat_3[1],
		data_2[0] * mat_0[2] + data_2[1] * mat_1[2] + data_2[2] * mat_2[2] + data_2[3] * mat_3[2],
		data_2[0] * mat_0[3] + data_2[1] * mat_1[3] + data_2[2] * mat_2[3] + data_2[3] * mat_3[3]
	};

	result[3] = std::array<T, 4>{
		data_3[0] * mat_0[0] + data_3[1] * mat_1[0] + data_3[2] * mat_2[0] + data_3[3] * mat_3[0],
		data_3[0] * mat_0[1] + data_3[1] * mat_1[1] + data_3[2] * mat_2[1] + data_3[3] * mat_3[1],
		data_3[0] * mat_0[2] + data_3[1] * mat_1[2] + data_3[2] * mat_2[2] + data_3[3] * mat_3[2],
		data_3[0] * mat_0[3] + data_3[1] * mat_1[3] + data_3[2] * mat_2[3] + data_3[3] * mat_3[3]
	};

	return result;
}

template<typename T>
inline Matrix4<T> Matrix4<T>::operator+(const Matrix4<T>& mat) const {
	Matrix4<T> res;

	for (size_t i{ 0 }; i < 4; ++i) {
		for (size_t j{ 0 }; j < 4; ++j) {
			res[i][j] = m_data[i][j] + mat[i][j];
		}
	}

	return res;
}

template<typename T>
inline Matrix4<T> Matrix4<T>::operator-(const Matrix4<T>& mat) const {
	Matrix4<T> res;

	for (size_t i{ 0 }; i < 4; ++i) {
		for (size_t j{ 0 }; j < 4; ++j) {
			res[i][j] = m_data[i][j] - mat[i][j];
		}
	}

	return res;
}

template<typename T>
inline bool Matrix4<T>::operator==(const Matrix4& mat) const {
	return m_data[0] == mat[0] && m_data[1] == mat[1] && m_data[2] == mat[2] && m_data[3] == mat[3];
}

template<typename T>
inline Matrix3<T> Matrix4<T>::toMatrix3(size_t row, size_t column) const {
	assert(row < 4 && column < 4);
	Matrix3<T> troncated_matrix{};

	size_t troncated_y = 0;
	for (size_t y = 0; y < 4; ++y) {
		if (y == row) { continue; }

		size_t troncated_x = 0;
		for (size_t x = 0; x < 4; ++x) {
			if (x == column) { continue; }
			troncated_matrix[troncated_y][troncated_x] = m_data[y][x];
			troncated_x += 1;
		}

		troncated_y += 1;
	}

	return troncated_matrix;
}

template<typename T>
inline float Matrix4<T>::det() const {
	return m_data[0][0] * (m_data[1][1] * m_data[2][2] * m_data[3][3] - m_data[1][1] * m_data[2][3] * m_data[3][2] - m_data[1][2] * m_data[2][1] * m_data[3][3] + m_data[1][2] * m_data[2][3] * m_data[3][1] + m_data[1][3] * m_data[2][1] * m_data[3][2] - m_data[1][3] * m_data[2][2] * m_data[3][1])
		 - m_data[0][1] * (m_data[1][0] * m_data[2][2] * m_data[3][3] - m_data[1][0] * m_data[2][3] * m_data[3][2] - m_data[1][2] * m_data[2][0] * m_data[3][3] + m_data[1][2] * m_data[2][3] * m_data[3][0] + m_data[1][3] * m_data[2][0] * m_data[3][2] - m_data[1][3] * m_data[2][2] * m_data[3][0])
		 + m_data[0][2] * (m_data[1][0] * m_data[2][1] * m_data[3][3] - m_data[1][0] * m_data[2][3] * m_data[3][1] - m_data[1][1] * m_data[2][0] * m_data[3][3] + m_data[1][1] * m_data[2][3] * m_data[3][0] + m_data[1][3] * m_data[2][0] * m_data[3][1] - m_data[1][3] * m_data[2][1] * m_data[3][0])
		 - m_data[0][3] * (m_data[1][0] * m_data[2][1] * m_data[3][2] - m_data[1][0] * m_data[2][2] * m_data[3][1] - m_data[1][1] * m_data[2][0] * m_data[3][2] + m_data[1][1] * m_data[2][2] * m_data[3][0] + m_data[1][2] * m_data[2][0] * m_data[3][1] - m_data[1][2] * m_data[2][1] * m_data[3][0]);
}

template<typename T>
inline Matrix4<T> Matrix4<T>::adj() const {
	return Matrix4{
		toMatrix3(0, 0).det(), -toMatrix3(1, 0).det(), toMatrix3(2, 0).det(), -toMatrix3(3, 0).det(),
		-toMatrix3(0, 1).det(), toMatrix3(1, 1).det(), -toMatrix3(2, 1).det(), toMatrix3(3, 1).det(),
		toMatrix3(0, 2).det(), -toMatrix3(1, 2).det(), toMatrix3(2, 2).det(), -toMatrix3(3, 2).det(),
		-toMatrix3(0, 3).det(), toMatrix3(1, 3).det(), -toMatrix3(2, 3).det(), toMatrix3(3, 3).det(),
	};
}

template<typename T>
inline Matrix4<T> Matrix4<T>::inv() const {
	assert(det() != 0);
	return (1/det()) * adj();
}

template<typename T>
inline Matrix4<T> Matrix4<T>::Identity() {
	return Matrix4<T>{
		T(1), T(0), T(0), T(0),
		T(0), T(1), T(0), T(0),
		T(0), T(0), T(1), T(0),
		T(0), T(0), T(0), T(1)
	};
}

template<typename T>
inline Matrix4<T> Matrix4<T>::Perspective(float fovAngle, float aspectRatio, float nearClipping, float farClipping) {
	fovAngle /= static_cast<T>(2.0);

	//const float scale{ std::sin(fovAngle) };
	float S = 1 / std::tan(fovAngle);

	return Matrix4<T>{
			static_cast<T>(S), static_cast<T>(0.f), static_cast<T>(0.0), static_cast<T>(0.0),
			static_cast<T>(0.f), static_cast<T>(S), static_cast<T>(0.f), static_cast<T>(0.f),
			static_cast<T>(0.f), static_cast<T>(0.f),- (farClipping) / (farClipping - nearClipping), -1.f,
			static_cast<T>(0.f), static_cast<T>(0.f), -(farClipping * nearClipping) / (farClipping - nearClipping), static_cast<T>(0.f),
	};
}

template<typename T>
inline Matrix4<T> Matrix4<T>::Translation(const Vec3<T>& translation) {
	return Matrix4<T>{
			static_cast<T>(1.0), static_cast<T>(0.0), static_cast<T>(0.0), translation.x,
			static_cast<T>(0.0), static_cast<T>(1.0), static_cast<T>(0.0), translation.y,
			static_cast<T>(0.0), static_cast<T>(0.0), static_cast<T>(1.0), translation.z,
			static_cast<T>(0.0), static_cast<T>(0.0), static_cast<T>(0.0), static_cast<T>(1.0)
	};
}

template<typename T>
inline Matrix4<T> Matrix4<T>::Rotation(float angle, Vec3<T> axis) {
	axis.normalize();

	const float c = std::cos(angle);
	const float s = std::sin(angle);

	Vec3<T> temp{ axis * (T(1) - c) };

	Matrix4<T> rotate;
	rotate[0][0] = c + temp.x * axis.x;
	rotate[0][1] = 0 + temp.x * axis.y + s * axis.z;
	rotate[0][2] = 0 + temp.x * axis.z - s * axis.y;

	rotate[1][0] = 0 + temp.y * axis.x - s * axis.z;
	rotate[1][1] = c + temp.y * axis.y;
	rotate[1][2] = 0 + temp.y * axis.z + s * axis.x;

	rotate[2][0] = 0 + temp.z * axis.x + s * axis.y;
	rotate[2][1] = 0 + temp.z * axis.y - s * axis.x;
	rotate[2][2] = c + temp.z * axis.z;

	Matrix4<T> result;
	result[0][0] = rotate[0][0];
	result[1][0] = rotate[0][1];
	result[2][0] = rotate[0][2];
	result[3][0] = static_cast<T>(0.f);

	result[0][1] = rotate[1][0];
	result[1][1] = rotate[1][1];
	result[2][1] = rotate[1][2];
	result[3][1] = static_cast<T>(0.f);

	result[0][2] = rotate[2][0];
	result[1][2] = rotate[2][1];
	result[2][2] = rotate[2][2];
	result[3][2] = static_cast<T>(0.f);

	result[0][3] = static_cast<T>(0.f);
	result[1][3] = static_cast<T>(0.f);
	result[2][3] = static_cast<T>(0.f);
	result[3][3] = static_cast<T>(1.f);

	return result;
}

template<typename T>
inline Matrix4<T> Matrix4<T>::Viewport(int x, int y, unsigned w, unsigned h, unsigned depth) {
	Matrix4<T> m = Matrix4<T>::Identity();
	m[0][3] = T(x + w / 2.f);
	m[1][3] = T(y + h / 2.f);
	m[2][3] = T(depth / 2.f);

	m[0][0] = T(w / 2.f);
	m[1][1] = T(h / 2.f);
	m[2][2] = T(depth / 2.f);

	return m;
}

template<typename T>
inline Matrix4<T> Matrix4<T>::FromVec(const Vec3<T>& vec) {
	return Matrix4<T>{
		vec.x, T(0), T(0), T(0),
		vec.y, T(0), T(0), T(0),
		vec.z, T(0), T(0), T(0),
		T(1), T(0), T(0), T(0)
	};
}

template<typename T>
inline Matrix4<T> operator*(const Matrix4<T>& mat, T scalar) {
	return Matrix4<T>{
		scalar * mat[0][0], scalar * mat[0][1], scalar * mat[0][2], scalar * mat[0][3],
		scalar * mat[1][0], scalar * mat[1][1], scalar * mat[1][2], scalar * mat[1][3],
		scalar * mat[2][0], scalar * mat[2][1], scalar * mat[2][2], scalar * mat[2][3],
		scalar * mat[3][0], scalar * mat[3][1], scalar * mat[3][2], scalar * mat[3][3]
	};
}

template<typename T>
inline Matrix4<T> operator*(T scalar, const Matrix4<T>& mat) {
	return mat * scalar;
}

template<typename T>
inline Vec4<T> operator*(const Matrix4<T>& mat, const Vec4<T>& vec) {
	return Vec4<T>{ 
		mat[0][0] * vec.x + mat[0][1] * vec.y + mat[0][2] * vec.z + mat[0][3] * vec.w,
		mat[1][0] * vec.x + mat[1][1] * vec.y + mat[1][2] * vec.z + mat[1][3] * vec.w,
		mat[2][0] * vec.x + mat[2][1] * vec.y + mat[2][2] * vec.z + mat[2][3] * vec.w,
		mat[3][0] * vec.x + mat[3][1] * vec.y + mat[3][2] * vec.z + mat[3][3] * vec.w,
	};
}

template<typename T>
std::ostream& operator<<(std::ostream& out, const Matrix4<T>& mat) {
	out << "Matrix4(";
	for (size_t i{ 0 }; i < 4; ++i) {
		for (size_t j{ 0 }; j < 4; ++j) {
			out << mat[i][j] << ", ";
		}

		out << "; ";
	}

	out << ")";

	return out;
}
