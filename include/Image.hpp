// *****************************************************************************
// STD Renderer

// Authors: Maxime Rambosson
// Contact: Maxime.Rambosson@etu.unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#ifndef IMAGE_HPP
#define IMAGE_HPP

#include <fstream>
#include <algorithm>
#include <atomic>

#pragma pack(push,1)
struct TGA_Header {
	char idlength;
	char colormaptype;
	char datatypecode;
	short colormaporigin;
	short colormaplength;
	char colormapdepth;
	short x_origin;
	short y_origin;
	short width;
	short height;
	char  bitsperpixel;
	char  imagedescriptor;
};
#pragma pack(pop)



struct TGAColor {
	struct RGBA {
		unsigned char b, g, r, a;
		RGBA(unsigned char R, unsigned char G, unsigned char B, unsigned char A) :
			b(B), g(G), r(R), a(A) { }
	};

	union {
		RGBA values;
		unsigned char raw[4];
		unsigned int val;
	};
	int bytespp;

	TGAColor() : val(0), bytespp(1) {
	}

	TGAColor(unsigned char R, unsigned char G, unsigned char B, unsigned char A) : values(R, G, B, A), bytespp(4) {
	}

	TGAColor(int v, int bpp) : val(v), bytespp(bpp) {
	}

	TGAColor(const TGAColor &c) : val(c.val), bytespp(c.bytespp) {
	}

	TGAColor(const unsigned char *p, int bpp) : val(0), bytespp(bpp) {
		for (int i=0; i<bpp; i++) {
			raw[i] = p[i];
		}
	}

	TGAColor& operator=(const TGAColor& c) {
		if (this != &c) {
			bytespp = c.bytespp;
			val = c.val;
		}
		return *this;
	}

	TGAColor operator+(const TGAColor& c) const {
		return TGAColor{ (unsigned char)(std::min(values.r + c.values.r, 255)), (unsigned char)(std::min(values.g + c.values.g, 255)), (unsigned char)(std::min(values.b + c.values.b, 255)), values.a };
	}

	TGAColor operator-(const TGAColor& c) const {
		return TGAColor{ (values.r < c.values.r) ? (unsigned char)(values.r - c.values.r) : (unsigned char)(0),
						 (values.g < c.values.g) ? (unsigned char)(values.g - c.values.g) : (unsigned char)(0),
						 (values.b < c.values.b) ? (unsigned char)(values.b - c.values.b) : (unsigned char)(0),
						 values.a };
	}

	TGAColor operator*(float scalar) const {
		return TGAColor{ (unsigned char)(values.r * scalar), (unsigned char)(values.g * scalar), (unsigned char)(values.b * scalar), values.a };
	}
};

std::ostream& operator<<(std::ostream& out, const TGAColor& color);


class TGAImage {
protected:
	unsigned char* data;
	short width;
	short height;
	short bytespp;

	bool load_rle_data(std::ifstream &in);
	bool unload_rle_data(std::ofstream &out);
public:
	enum Format {
		GRAYSCALE=1, RGB=3, RGBA=4
	};

	TGAImage();
	TGAImage(short w, short h, short bpp);
	TGAImage(const TGAImage &img);
	bool read_tga_file(const char *filename);
	bool write_tga_file(const char *filename, bool rle=true);
	bool flip_horizontally();
	bool flip_vertically();
	bool scale(short w, short h);
	inline TGAColor get(int x, int y);
	inline bool set(int x, int y, TGAColor c);
	~TGAImage();
	TGAImage & operator =(const TGAImage &img);
	inline int get_width() const;
	inline int get_height() const;
	inline int get_bytespp() const;
	inline unsigned char *buffer();
	void clear();
};

#include <Image.inl>

#endif
