// *****************************************************************************
// STD Renderer

// Authors: Maxime Rambosson
// Contact: Maxime.Rambosson@etu.unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#ifndef MODEL_HPP
#define MODEL_HPP

#include <Geometry.hpp>
#include <vector>
#include <memory>
#include <map>

class Model {
public:
	Model() = default;
	~Model() = default;

	inline size_t nverts() const;
	inline size_t nfaces() const;
	inline Vec3f vert(size_t i) const;
	inline const std::vector<std::vector<size_t>>& faces() const;
	inline std::vector<size_t> face(size_t idx) const;

	void add_face(const std::vector<Vec3f>& verts);

	static std::unique_ptr<Model> From_OBJ(const std::string& filename);

private:
	std::vector<Vec3f> m_verts;
	std::map<Vec3f, size_t> m_verts_cache;
	std::vector<std::vector<size_t>> m_faces;
};

#include <Model.inl>

#endif
