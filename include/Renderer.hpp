// *****************************************************************************
// STD Renderer

// Authors: Maxime Rambosson
// Contact: Maxime.Rambosson@etu.unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#ifndef RENDERER_HPP
#define RENDERER_HPP

#include <Model.hpp>
#include <Geometry.hpp>
#include <vector>
#include <memory>
#include <functional>

class TGAImage;
class Shader;
struct TGAColor;

class Renderer{
public:
	Renderer();
	~Renderer() = default;

	size_t add_model(std::unique_ptr<Model>&& model);

	void set_view_matrix(const Matrix4f& view);
	void set_perspective_projection(bool active);

	void render(std::unique_ptr<TGAImage>& surface, const std::unique_ptr<Shader>& shader) const;
	void blend_render(std::unique_ptr<TGAImage>& surface, const std::unique_ptr<Shader>& shader) const;
	void depth_render(std::unique_ptr<TGAImage>& surface, const std::unique_ptr<Shader>& shader) const;

private:
	void draw_model(TGAImage* surface, const Model* model, const Shader* shader, const Matrix4f* VP) const;
	void draw_model_on_blendbuffer(TGAColor* surface, std::atomic<unsigned>* indices, const Model* model, const Shader* shader, const Matrix4f* VP, int width, int height) const;
	void draw_model_on_depthbuffer(TGAColor* surface, float* depths, std::atomic<unsigned>* indices, const Model* model, const Shader* shader, const Matrix4f* VP, int width, int height) const;

	void mark_model_pixels(std::atomic<unsigned>* triangle_counts, const Model* model, const Matrix4f* VP, int width, int height) const;
	void mark_pixels(std::atomic<unsigned>* triangle_counts, const Model* model, const std::vector<size_t>& vertices, const Matrix4f* VP, int width, int height) const;
	
	void draw_face(TGAImage* surface, const Model* model, const Shader* shader, const std::vector<size_t>& vertices, const Matrix4f* VP) const;
	void draw_blended_face(TGAColor* colors, std::atomic<unsigned>* indices, const Model* model, const Shader* shader, const std::vector<size_t>& vertices, const Matrix4f* VP, int width, int height) const;
	void draw_depth_buffered_face(TGAColor* surface, float* depths, std::atomic<unsigned>* indices, const Model* model, const Shader* shader, const std::vector<size_t>& vertices, const Matrix4f* VP, int width, int height) const;
	
	void draw_triangle(TGAImage* surface, const std::array<Vec3i, 3>& pts, const std::array<TGAColor, 3>& colors) const;
	void draw_blended_triangle(TGAColor* colors, std::atomic<unsigned>* indices, const std::array<Vec3i, 3>& pts, const std::array<TGAColor, 3>& v_colors, int width, int height) const;
	void draw_depth_buffered_triangle(TGAColor* surface, float* depths, std::atomic<unsigned>* indices, const std::array<Vec3i, 3>& pts, const std::array<TGAColor, 3>& colors, int width, int height) const;

	void draw_blended_frame(TGAImage* surface, const TGAColor* colors, const unsigned* indices_start, const std::atomic<unsigned>* indices_end, const std::atomic<unsigned>& current_indice, unsigned width) const ;
	void draw_depth_buffered_frame(TGAImage* surface, const TGAColor* colors, const float* depths, const unsigned* indices_start, const std::atomic<unsigned>* indices_end, const std::atomic<unsigned>& current_indice, unsigned width) const;

	std::vector<std::unique_ptr<Model>> m_models;
	Matrix4f m_view;
	Matrix4f m_projection;
};


#endif
