CXX=g++
GXX=nvc++
INC_DIR = -I include/.
BUILD_DIR = build/linux/
OPT = -std=c++17 -Wall -Wextra -pedantic -Werror -ltbb -DPARALLEL_EXEC
GPU_OPT = -stdpar -lcudart

dir_guard = @mkdir -p $(BUILD_DIR)

cpu_Image.o:
	$(dir_guard)
	$(CXX) -o $(BUILD_DIR)Image.o $(OPT) $(INC_DIR) -c src/Image.cpp

cpu_Model.o:
	$(dir_guard)
	$(CXX) -o $(BUILD_DIR)Model.o $(OPT) $(INC_DIR) -c src/Model.cpp

cpu_Utils.o: cpu_Model.o
	$(dir_guard)
	$(CXX) -o $(BUILD_DIR)Utils.o $(OPT) $(INC_DIR) -c src/Utils.cpp

gpu_Image.o:
	$(dir_guard)
	$(GXX) -o $(BUILD_DIR)Image.o $(GPU_OPT) $(OPT) $(INC_DIR) -c src/Image.cpp

gpu_Model.o:
	$(dir_guard)
	$(GXX) -o $(BUILD_DIR)Model.o $(GPU_OPT) $(OPT) $(INC_DIR) -c src/Model.cpp

gpu_Utils.o: gpu_Model.o
	$(dir_guard)
	$(GXX) -o $(BUILD_DIR)Utils.o $(GPU_OPT) $(OPT) $(INC_DIR) -c src/Utils.cpp

cpu_dummy_render: cpu_Image.o cpu_Model.o cpu_Utils.o
	$(dir_guard)
	$(CXX) -o $(BUILD_DIR)Shader.o $(OPT) $(INC_DIR) -I exemples/dummy_render/ -c exemples/dummy_render/Shader.cpp
	$(CXX) -o $(BUILD_DIR)Renderer.o $(OPT) $(INC_DIR) -I exemples/dummy_render/ -c src/Renderer.cpp
	$(CXX) -o $(BUILD_DIR)main.o $(OPT) $(INC_DIR) -I exemples/dummy_render/ -c exemples/dummy_render/main.cpp
	$(CXX) $(BUILD_DIR)main.o $(BUILD_DIR)Utils.o $(BUILD_DIR)Image.o $(BUILD_DIR)Model.o $(BUILD_DIR)Shader.o  $(BUILD_DIR)Renderer.o -o $(BUILD_DIR)cpu_dummy_render -ltbb

gpu_dummy_render: gpu_Image.o gpu_Model.o gpu_Utils.o
	$(dir_guard)
	$(GXX) -o $(BUILD_DIR)Shader.o $(GPU_OPT) $(OPT) $(INC_DIR) -I exemples/dummy_render/  -c exemples/dummy_render/Shader.cpp
	$(GXX) -o $(BUILD_DIR)Renderer.o $(GPU_OPT) $(OPT) $(INC_DIR) -I exemples/dummy_render/ -c src/Renderer.cpp
	$(GXX) -o $(BUILD_DIR)main.o $(GPU_OPT) $(OPT) $(INC_DIR) -I exemples/dummy_render/ -c exemples/dummy_render/main.cpp
	$(GXX) $(GPU_OPT) $(BUILD_DIR)Utils.o $(BUILD_DIR)Image.o $(BUILD_DIR)Shader.o  $(BUILD_DIR)main.o $(BUILD_DIR)Renderer.o $(BUILD_DIR)Model.o -o $(BUILD_DIR)gpu_dummy_render

cpu_selfcontained_cavity_render: cpu_Image.o cpu_Model.o cpu_Utils.o
	$(dir_guard)
	$(CXX) -o $(BUILD_DIR)Shader.o $(OPT) $(INC_DIR) -I exemples/selfcontained_cavity_render/  -c exemples/selfcontained_cavity_render/Shader.cpp
	$(CXX) -o $(BUILD_DIR)Renderer.o $(OPT) $(INC_DIR) -I exemples/selfcontained_cavity_render/ -c src/Renderer.cpp
	$(CXX) -o $(BUILD_DIR)main.o $(OPT) $(INC_DIR) -I exemples/selfcontained_cavity_render/  -c exemples/selfcontained_cavity_render/selfcontained_cavity_aa_soa.cpp
	$(CXX) $(BUILD_DIR)main.o $(BUILD_DIR)Utils.o $(BUILD_DIR)Image.o $(BUILD_DIR)Shader.o $(BUILD_DIR)Model.o $(BUILD_DIR)Renderer.o -o $(BUILD_DIR)cpu_selfcontained_cavity_render -ltbb

gpu_selfcontained_cavity_render: gpu_Image.o gpu_Model.o gpu_Utils.o
	$(dir_guard)
	$(GXX) -o $(BUILD_DIR)Shader.o $(GPU_OPT) $(OPT) $(INC_DIR) -I exemples/selfcontained_cavity_render/  -c exemples/selfcontained_cavity_render/Shader.cpp
	$(GXX) -o $(BUILD_DIR)Renderer.o $(GPU_OPT) $(OPT) $(INC_DIR) -I exemples/selfcontained_cavity_render/ -c src/Renderer.cpp
	$(GXX) -o $(BUILD_DIR)main.o $(GPU_OPT) $(OPT) $(INC_DIR) -I exemples/selfcontained_cavity_render/ -c exemples/selfcontained_cavity_render/selfcontained_cavity_aa_soa.cpp
	$(GXX) $(GPU_OPT) $(BUILD_DIR)Utils.o $(BUILD_DIR)Image.o $(BUILD_DIR)Shader.o  $(BUILD_DIR)main.o $(BUILD_DIR)Renderer.o $(BUILD_DIR)Model.o -o $(BUILD_DIR)gpu_selfcontained_cavity_render

cpu_overlap: cpu_Image.o cpu_Model.o cpu_Utils.o
	$(dir_guard)
	$(CXX) -o $(BUILD_DIR)Shader.o $(OPT) $(INC_DIR) -I exemples/overlap/  -c exemples/overlap/Shader.cpp
	$(CXX) -o $(BUILD_DIR)Renderer.o $(OPT) $(INC_DIR) -I exemples/overlap/ -c src/Renderer.cpp
	$(CXX) -o $(BUILD_DIR)main.o $(OPT) $(INC_DIR) -I exemples/overlap/  -c exemples/overlap/main.cpp
	$(CXX) $(BUILD_DIR)main.o $(BUILD_DIR)Utils.o $(BUILD_DIR)Image.o $(BUILD_DIR)Shader.o $(BUILD_DIR)Model.o $(BUILD_DIR)Renderer.o -o $(BUILD_DIR)cpu_overlap -ltbb

gpu_overlap: gpu_Image.o gpu_Model.o gpu_Utils.o
	$(dir_guard)
	$(GXX) -o $(BUILD_DIR)Shader.o $(GPU_OPT) $(OPT) $(INC_DIR) -I exemples/overlap/  -c exemples/overlap/Shader.cpp
	$(GXX) -o $(BUILD_DIR)Renderer.o $(GPU_OPT) $(OPT) $(INC_DIR) -I exemples/overlap/ -c src/Renderer.cpp
	$(GXX) -o $(BUILD_DIR)main.o $(GPU_OPT) $(OPT) $(INC_DIR) -I exemples/overlap/  -c exemples/overlap/main.cpp
	$(GXX) $(GPU_OPT) $(BUILD_DIR)main.o $(BUILD_DIR)Utils.o $(BUILD_DIR)Image.o $(BUILD_DIR)Shader.o $(BUILD_DIR)Model.o $(BUILD_DIR)Renderer.o -o $(BUILD_DIR)gpu_overlap

cpu_model: cpu_Image.o cpu_Model.o cpu_Utils.o
	$(dir_guard)
	$(CXX) -o $(BUILD_DIR)Shader.o $(OPT) $(INC_DIR) -I exemples/model/  -c exemples/model/Shader.cpp
	$(CXX) -o $(BUILD_DIR)Renderer.o $(OPT) $(INC_DIR) -I exemples/model/ -c src/Renderer.cpp
	$(CXX) -o $(BUILD_DIR)main.o $(OPT) $(INC_DIR) -I exemples/model/  -c exemples/model/main.cpp
	$(CXX) $(BUILD_DIR)main.o $(BUILD_DIR)Utils.o $(BUILD_DIR)Image.o $(BUILD_DIR)Shader.o $(BUILD_DIR)Model.o $(BUILD_DIR)Renderer.o -o $(BUILD_DIR)cpu_model -ltbb

gpu_model: gpu_Image.o gpu_Model.o gpu_Utils.o
	$(dir_guard)
	$(GXX) -o $(BUILD_DIR)Shader.o $(GPU_OPT) $(OPT) $(INC_DIR) -I exemples/model/  -c exemples/model/Shader.cpp
	$(GXX) -o $(BUILD_DIR)Renderer.o $(GPU_OPT) $(OPT) $(INC_DIR) -I exemples/model/ -c src/Renderer.cpp
	$(GXX) -o $(BUILD_DIR)main.o $(GPU_OPT) $(OPT) $(INC_DIR) -I exemples/model/  -c exemples/model/main.cpp
	$(GXX) $(GPU_OPT) $(BUILD_DIR)main.o $(BUILD_DIR)Utils.o $(BUILD_DIR)Image.o $(BUILD_DIR)Shader.o $(BUILD_DIR)Model.o $(BUILD_DIR)Renderer.o -o $(BUILD_DIR)gpu_model
