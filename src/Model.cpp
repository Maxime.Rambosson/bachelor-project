// *****************************************************************************
// STD Renderer

// Authors: Maxime Rambosson
// Contact: Maxime.Rambosson@etu.unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#include <Model.hpp>

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>


void Model::add_face(const std::vector<Vec3f>& verts) {
	std::vector<size_t> face;
	for (const auto& vert : verts) {
		const auto search = m_verts_cache.find(vert);
		if (search == m_verts_cache.cend()) {
			face.push_back(m_verts.size());
			m_verts_cache[vert] = m_verts.size();
			m_verts.push_back(vert);
		}
		else {
			face.push_back(search->second);
		}

	}

	m_faces.push_back(face);
}

std::unique_ptr<Model> Model::From_OBJ(const std::string& filename) {
	std::unique_ptr<Model> model{ std::make_unique<Model>() };

	std::ifstream in;
	in.open(filename, std::ifstream::in);
	if (in.fail()) return std::make_unique<Model>();

	std::string line;
	while (!in.eof()) {
		std::getline(in, line);
		std::istringstream iss(line.c_str());
		char trash;
		if (!line.compare(0, 2, "v ")) {
			iss >> trash;
			Vec3f v;
			for (int i = 0; i < 3; i++) iss >> v[i];
			model->m_verts.push_back(v);
		}
		else if (!line.compare(0, 2, "f ")) {
			std::vector<size_t> f;
			size_t itrash, idx;
			iss >> trash;
			while (iss >> idx >> trash >> itrash >> trash >> itrash) {
				idx--; // in wavefront obj all indices start at 1, not zero
				f.push_back(idx);
			}
			model->m_faces.push_back(f);
		}
	}
	std::cerr << "# v# " << model->nverts() << " f# " << model->nfaces() << std::endl;

	return model;
}
