// *****************************************************************************
// STD Renderer

// Authors: Maxime Rambosson
// Contact: Maxime.Rambosson@etu.unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#include <Renderer.hpp>

#include <Image.hpp>
#include <Geometry.hpp>
#include <Shader.hpp>

#include <algorithm>
#include <execution>
#include <numeric>

Renderer::Renderer() : m_view( Matrix4f::Identity() ), m_projection(Matrix4f::Identity()) {
	//Matrix4f Projection = Matrix4f::Perspective(to_radiant(70.f), (float)(width) / (float)(height), 0.1f, 200.f);
	m_projection[3][2] = -1.f;
}

size_t Renderer::add_model(std::unique_ptr<Model>&& model) {
	m_models.emplace_back(std::move(model));

	return m_models.size() - 1;
}

void Renderer::set_view_matrix(const Matrix4f& view) {
	m_view = view;
}

void Renderer::set_perspective_projection(bool active) {
	if(active){
		m_projection[3][2] = -1.f;
	}
	else {
		m_projection[3][2] = 0.f; // Recreate identity matrix
	}
}


void Renderer::render(std::unique_ptr<TGAImage>& surface, const std::unique_ptr<Shader>& shader) const {
	const int width = surface->get_width();
	const int height = surface->get_height();

	Matrix4f ViewPort = Matrix4f::Viewport(width / 8, height / 8, width * 3 / 4, height * 3 / 4, 255);
	std::unique_ptr<Matrix4f> VP{ std::make_unique<Matrix4f>(ViewPort * m_projection * m_view) };

	for (size_t i = 0; i < m_models.size(); i++) {
		draw_model(surface.get(), m_models[i].get(), shader.get(), VP.get());
	}
}

void Renderer::blend_render(std::unique_ptr<TGAImage>& surface, const std::unique_ptr<Shader>& shader) const {
	const int width = surface->get_width();
	const int height = surface->get_height();

	Matrix4f ViewPort = Matrix4f::Viewport(width / 8, height / 8, width * 3 / 4, height * 3 / 4, 255);
	std::unique_ptr<Matrix4f> VP{ std::make_unique<Matrix4f>(ViewPort * m_projection * m_view) };

	std::vector<std::atomic<unsigned>> triangle_counts(surface->get_height() * surface->get_width());
	for (size_t i = 0; i < m_models.size(); i++) {
		mark_model_pixels(triangle_counts.data(), m_models[i].get(), VP.get(), surface->get_width(), surface->get_height());
	}

	std::vector<unsigned> indices_start(triangle_counts.size());
	std::exclusive_scan(triangle_counts.begin(), triangle_counts.end(), indices_start.begin(), 0, std::plus<unsigned>{});
	std::vector<std::atomic<unsigned>> indices_end(indices_start.begin(), indices_start.end());

	std::vector<TGAColor> colors(indices_start[indices_start.size() - 1] + triangle_counts[triangle_counts.size() - 1]);

	for (size_t i = 0; i < m_models.size(); i++) {
		draw_model_on_blendbuffer(colors.data(), indices_end.data(), m_models[i].get(), shader.get(), VP.get(), surface->get_width(), surface->get_height());
	}

	std::for_each(
		#if defined(PARALLEL_EXEC)
		std::execution::par_unseq,
		#endif
		indices_end.cbegin(), indices_end.cend(), [surface_ptr = surface.get(), colors_ptr = colors.data(), indices_start_ptr = indices_start.data(), indices_end_ptr = indices_end.data(), width = surface->get_width(), this](const std::atomic<unsigned>& indice) {
			draw_blended_frame(surface_ptr, colors_ptr, indices_start_ptr, indices_end_ptr, indice, width);
		}
	);

}

void Renderer::depth_render(std::unique_ptr<TGAImage>& surface, const std::unique_ptr<Shader>& shader) const {
	const int width = surface->get_width();
	const int height = surface->get_height();

	Matrix4f ViewPort = Matrix4f::Viewport(width / 8, height / 8, width * 3 / 4, height * 3 / 4, 255);
	std::unique_ptr<Matrix4f> VP{ std::make_unique<Matrix4f>(ViewPort * m_projection * m_view) };

	std::vector<std::atomic<unsigned>> triangle_counts(surface->get_height() * surface->get_width());
	for (size_t i = 0; i < m_models.size(); i++) {
		mark_model_pixels(triangle_counts.data(), m_models[i].get(), VP.get(), surface->get_width(), surface->get_height());
	}

	std::vector<unsigned> indices_start(triangle_counts.size());
	std::exclusive_scan(triangle_counts.begin(), triangle_counts.end(), indices_start.begin(), 0, std::plus<unsigned>{});
	std::vector<std::atomic<unsigned>> indices_end(indices_start.begin(), indices_start.end());

	std::vector<TGAColor> colors(indices_start[indices_start.size() - 1] + triangle_counts[triangle_counts.size() - 1]);
	std::vector<float> depths(indices_start[indices_start.size() - 1] + triangle_counts[triangle_counts.size() - 1]);

	for (size_t i = 0; i < m_models.size(); i++) {
		draw_model_on_depthbuffer(colors.data(), depths.data(), indices_end.data(), m_models[i].get(), shader.get(), VP.get(), surface->get_width(), surface->get_height());
	}

	std::for_each(
		#if defined(PARALLEL_EXEC)
		std::execution::par_unseq,
		#endif
		indices_end.cbegin(), indices_end.cend(), [surface_ptr = surface.get(), colors_ptr = colors.data(), depths_ptr = depths.data(), indices_start_ptr = indices_start.data(), indices_end_ptr = indices_end.data(), width = surface->get_width(), this](const std::atomic<unsigned>& indice) {
			draw_depth_buffered_frame(surface_ptr, colors_ptr, depths_ptr, indices_start_ptr, indices_end_ptr, indice, width);
		}
	);
}

void Renderer::draw_model(TGAImage* surface, const Model* model, const Shader* shader, const Matrix4f* VP) const {
	const std::vector<std::vector<size_t>>& faces{ model->faces() };

	std::for_each(
		#if defined(PARALLEL_EXEC)
		std::execution::par_unseq,
		#endif
		faces.cbegin(), faces.cend(), [surface, model, shader, VP, this](const std::vector<size_t>& vertices) {
			draw_face(surface, model, shader, vertices, VP);
		}
	);

	
}

void Renderer::draw_model_on_blendbuffer(TGAColor* surface, std::atomic<unsigned>* indices, const Model* model, const Shader* shader, const Matrix4f* VP, int width, int height) const {
	const std::vector<std::vector<size_t>>& faces{ model->faces() };

	std::for_each(
		#if defined(PARALLEL_EXEC)
		std::execution::par_unseq,
		#endif
		faces.cbegin(), faces.cend(), [surface, indices, model, shader, VP, width, height, this](const std::vector<size_t>& vertices) {
			draw_blended_face(surface, indices, model, shader, vertices, VP, width, height);
		}
	);
}

void Renderer::draw_model_on_depthbuffer(TGAColor* surface, float* depths, std::atomic<unsigned>* indices, const Model* model, const Shader* shader, const Matrix4f* VP, int width, int height) const {
	const std::vector<std::vector<size_t>>& faces{ model->faces() };
	
	std::for_each(
		#if defined(PARALLEL_EXEC)
		std::execution::par_unseq,
		#endif
		faces.cbegin(), faces.cend(), [surface, depths, indices, model, shader, VP, width, height, this](const std::vector<size_t>& vertices) {
			draw_depth_buffered_face(surface, depths, indices, model, shader, vertices, VP, width, height);
		}
	);
}

void Renderer::mark_model_pixels(std::atomic<unsigned>* triangle_counts, const Model* model, const Matrix4f* VP, int width, int height) const {
	const std::vector<std::vector<size_t>>& faces{ model->faces() };
	
	std::for_each(
		#if defined(PARALLEL_EXEC)
		std::execution::par_unseq,
		#endif
		faces.cbegin(), faces.cend(), [triangle_counts, model, VP, width, height, this](const std::vector<size_t>& vertices) {
			mark_pixels(triangle_counts, model, vertices, VP, width, height);
		}
	);
}

void Renderer::mark_pixels(std::atomic<unsigned>* triangle_counts, const Model* model, const std::vector<size_t>& vertices, const Matrix4f* VP, int width, int height) const {
	std::array<Vec3i, 3> pts;

	for (size_t j = 0; j < 3; ++j) {
		Vec3f v = model->vert(vertices[j]);
		pts[j] = Vec3f::FromMatrix((*VP) * Matrix4f::FromVec(v));
	}

	Vec2i bboxmin{ std::numeric_limits<int>::max(), std::numeric_limits<int>::max() };
	Vec2i bboxmax{ -std::numeric_limits<int>::max(), -std::numeric_limits<int>::max() };
	Vec2i clamp{ width - 1, height - 1 };
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 2; ++j) {
			bboxmin[j] = std::max(0, std::min(bboxmin[j], pts[i][j]));
			bboxmax[j] = std::min(clamp[j], std::max(bboxmax[j], pts[i][j]));
		}
	}
	Vec3f P;
	for (P.y = float(bboxmin.y); P.y <= bboxmax.y; P.y++) {
		for (P.x = float(bboxmin.x); P.x <= bboxmax.x; P.x++) {
			Vec3f bc_screen = barycentric(pts[0], pts[1], pts[2], P);
			if (bc_screen.x < 0 || bc_screen.y < 0 || bc_screen.z < 0) continue;

			triangle_counts[(int)(P.y) * width + (int)(P.x)] += 1;
		}
	}
}

void Renderer::draw_face(TGAImage* surface, const Model* model, const Shader* shader, const std::vector<size_t>& vertices, const Matrix4f* VP) const {
	std::array<Vec3i, 3> pts;
	std::array<Vec3f, 3> model_coords;

	for (size_t i = 0; i < 3; ++i) {
		Vec3f v = model->vert(vertices[i]);
		model_coords[i] = v;
		pts[i] = Vec3f::FromMatrix((*VP) * Matrix4f::FromVec(v));
	}

	std::array<TGAColor, 3> colors;
	if (shader->fragment(model_coords, colors)) {
		return;
	};

	draw_triangle(surface, pts, colors);
}

void Renderer::draw_blended_face(TGAColor* colors, std::atomic<unsigned>* indices, const Model* model, const Shader* shader, const std::vector<size_t>& vertices, const Matrix4f* VP, int width, int height) const {
	std::array<Vec3i, 3> pts;
	std::array<Vec3f, 3> model_coords;
	
	for (size_t i = 0; i < 3; ++i) {
		Vec3f v = model->vert(vertices[i]);
		model_coords[i] = v;
		pts[i] = Vec3f::FromMatrix((*VP) * Matrix4f::FromVec(v));
	}

	std::array<TGAColor, 3> v_colors;
	if (shader->fragment(model_coords, v_colors)) {
		return;
	}

	draw_blended_triangle(colors, indices, pts, v_colors, width, height);
}

void Renderer::draw_depth_buffered_face(TGAColor* surface, float* depths, std::atomic<unsigned>* indices, const Model* model, const Shader* shader, const std::vector<size_t>& vertices, const Matrix4f* VP, int width, int height) const {
	std::array<Vec3i, 3> pts;
	std::array<Vec3f, 3> model_coords;

	for (size_t i = 0; i < 3; ++i) {
		Vec3f v = model->vert(vertices[i]);
		model_coords[i] = v;
		pts[i] = Vec3f::FromMatrix((*VP) * Matrix4f::FromVec(v));
	}

	std::array<TGAColor, 3> colors;
	if (shader->fragment(model_coords, colors)) {
		return;
	}

	draw_depth_buffered_triangle(surface, depths, indices, pts, colors, width, height);
}

void Renderer::draw_triangle(TGAImage* surface, const std::array<Vec3i, 3>& pts, const std::array<TGAColor, 3>& colors) const {
	Vec2i bboxmin{ std::numeric_limits<int>::max(), std::numeric_limits<int>::max() };
	Vec2i bboxmax{ -std::numeric_limits<int>::max(), -std::numeric_limits<int>::max() };
	Vec2i clamp{ surface->get_width() - 1, surface->get_height() - 1 };
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 2; ++j) {
			bboxmin[j] = std::max(0, std::min(bboxmin[j], pts[i][j]));
			bboxmax[j] = std::min(clamp[j], std::max(bboxmax[j], pts[i][j]));
		}
	}
	Vec3f P;
	for (P.x = float(bboxmin.x); P.x <= bboxmax.x; P.x++) {
		for (P.y = float(bboxmin.y); P.y <= bboxmax.y; P.y++) {
			Vec3f bc_screen = barycentric(pts[0], pts[1], pts[2], P);
			if (bc_screen.x < 0 || bc_screen.y < 0 || bc_screen.z < 0) continue;
			P.z = 0;
			for (int i = 0; i < 3; i++) {
				P.z += pts[i][2] * bc_screen[i];
			}

			float a = triangle_area(pts[0], pts[1], pts[2]);
			float a0 = triangle_area(P, pts[0], pts[1]);
			float a1 = triangle_area(P, pts[1], pts[2]);
			float a2 = triangle_area(P, pts[2], pts[0]);

			TGAColor color0 = colors[0] * (a1 / a);
			TGAColor color1 = colors[1] * (a2 / a);
			TGAColor color2 = colors[2] * (a0 / a);

			surface->set(int(P.x), int(P.y), color0 + color1 + color2);
		}
	}
}

void Renderer::draw_blended_triangle(TGAColor* colors, std::atomic<unsigned>* indices, const std::array<Vec3i, 3>& pts, const std::array<TGAColor, 3>& v_colors, int width, int height) const {
	Vec2i bboxmin{ std::numeric_limits<int>::max(), std::numeric_limits<int>::max() };
	Vec2i bboxmax{ -std::numeric_limits<int>::max(), -std::numeric_limits<int>::max() };
	Vec2i clamp{ width - 1, height - 1 };
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 2; ++j) {
			bboxmin[j] = std::max(0, std::min(bboxmin[j], pts[i][j]));
			bboxmax[j] = std::min(clamp[j], std::max(bboxmax[j], pts[i][j]));
		}
	}

	Vec3f P;
	for (P.x = float(bboxmin.x); P.x <= bboxmax.x; P.x++) {
		for (P.y = float(bboxmin.y); P.y <= bboxmax.y; P.y++) {
			Vec3f bc_screen = barycentric(pts[0], pts[1], pts[2], P);
			if (bc_screen.x < 0 || bc_screen.y < 0 || bc_screen.z < 0) continue;

			float a = triangle_area(pts[0], pts[1], pts[2]);
			float a0 = triangle_area(P, pts[0], pts[1]);
			float a1 = triangle_area(P, pts[1], pts[2]);
			float a2 = triangle_area(P, pts[2], pts[0]);

			TGAColor color0 = v_colors[0] * (a1 / a);
			TGAColor color1 = v_colors[1] * (a2 / a);
			TGAColor color2 = v_colors[2] * (a0 / a);

			unsigned indice = indices[int(P.y) * height + int(P.x)]++;
			colors[indice] = TGAColor{ color0 + color1 + color2 };
		}
	}
}

void Renderer::draw_depth_buffered_triangle(TGAColor* colors, float* depths, std::atomic<unsigned>* indices, const std::array<Vec3i, 3>& pts, const std::array<TGAColor, 3>& v_colors, int width, int height) const {
	Vec2i bboxmin{ std::numeric_limits<int>::max(), std::numeric_limits<int>::max() };
	Vec2i bboxmax{ -std::numeric_limits<int>::max(), -std::numeric_limits<int>::max() };
	Vec2i clamp{ width - 1, height - 1 };
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 2; ++j) {
			bboxmin[j] = std::max(0, std::min(bboxmin[j], pts[i][j]));
			bboxmax[j] = std::min(clamp[j], std::max(bboxmax[j], pts[i][j]));
		}
	}

	Vec3f P;
	for (P.x = float(bboxmin.x); P.x <= bboxmax.x; P.x++) {
		for (P.y = float(bboxmin.y); P.y <= bboxmax.y; P.y++) {
			Vec3f bc_screen = barycentric(pts[0], pts[1], pts[2], P);
			if (bc_screen.x < 0 || bc_screen.y < 0 || bc_screen.z < 0) continue;
			P.z = 0;
			for (size_t i = 0; i < 3; ++i) {
				P.z += pts[i][2] * bc_screen[i];
			}

			float a = triangle_area(pts[0], pts[1], pts[2]);
			float a0 = triangle_area(P, pts[0], pts[1]);
			float a1 = triangle_area(P, pts[1], pts[2]);
			float a2 = triangle_area(P, pts[2], pts[0]);

			TGAColor color0 = v_colors[0] * (a1 / a);
			TGAColor color1 = v_colors[1] * (a2 / a);
			TGAColor color2 = v_colors[2] * (a0 / a);

			unsigned indice = indices[int(P.y) * height + int(P.x)]++;
			colors[indice] = TGAColor{ color0 + color1 + color2 };
			depths[indice] = P.z;
		}
	}
}

void Renderer::draw_blended_frame(TGAImage* surface, const TGAColor* colors, const unsigned* indices_start, const std::atomic<unsigned>* indices_end, const std::atomic<unsigned>& current_indice, unsigned width) const {
	if (current_indice == 0) {
		// Nothing to render, current_indice pointe to next unused colors/depth for this pixel
		return;
	}
	
	const unsigned indice_index = (unsigned int)(&current_indice - indices_end);
	unsigned range = current_indice.load() - indices_start[indice_index];
	if (range == 0) {
		return;
	}

	unsigned r_sum = 0;
	unsigned g_sum = 0;
	unsigned b_sum = 0;

	for (unsigned i = 0; i < range; ++i) {
		const TGAColor& color{ colors[current_indice.load() - 1 - i] };
		r_sum += color.values.r;
		g_sum += color.values.g;
		b_sum += color.values.b;
	}

	unsigned y = indice_index / width;
	unsigned x = indice_index % width;

	surface->set(x, y, TGAColor{ (unsigned char)(r_sum / range), (unsigned char)(g_sum / range), (unsigned char)(b_sum / range), 255 });
}

void Renderer::draw_depth_buffered_frame(TGAImage* surface, const TGAColor* colors, const float* depths, const unsigned* indices_start, const std::atomic<unsigned>* indices_end, const std::atomic<unsigned>& current_indice, unsigned width) const {
	if (current_indice == 0) {
		// Nothing to render, current_indice pointe to next unused colors/depth for this pixel
		return;
	}
	
	const unsigned indice_index = (unsigned int)(&current_indice - indices_end);
	unsigned range = current_indice.load() - indices_start[indice_index];
	if (range == 0) {
		return;
	}

	float min_depth{ depths[current_indice - 1] };
	TGAColor color{ colors[current_indice - 1] };
	for (unsigned i = 0; i < range; ++i) {
		const float depth{ depths[current_indice - 1 - i] };
		if (depth > min_depth) {
			min_depth = depth;
			color = colors[current_indice - 1 - i];
		}
	}

	unsigned y = indice_index / width;
	unsigned x = indice_index % width;

	surface->set(x, y, color);
}
