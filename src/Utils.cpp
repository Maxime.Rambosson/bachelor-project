// *****************************************************************************
// STD Renderer

// Authors: Maxime Rambosson
// Contact: Maxime.Rambosson@etu.unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#include <Utils.hpp>

#include <Model.hpp>

#include <cmath>

std::unique_ptr<Model> create_plane(const Vec3f& A, const Vec3f& B, const Vec3f& C, unsigned nb_division) {
	std::unique_ptr<Model> model{ std::make_unique<Model>() };

	Vec3f AB{ B - A };
	Vec3f AC{ C - A };

	Vec3f N{ cross(AB, AC) };
	Vec3f U{ AB.normalize() };
	Vec3f uN{ N.normalize() };
	Vec3f V{ cross(U, uN) };

	Vec3f u{ A + U };
	Vec3f v{ A + V };
	Vec3f n{ A + uN };

	Matrix4f S;
	S[0] = std::array<float, 4>{ A.x, u.x, v.x, n.x };
	S[1] = std::array<float, 4>{ A.y, u.y, v.y, n.y };
	S[2] = std::array<float, 4>{ A.z, u.z, v.z, n.z };
	S[3] = std::array<float, 4>{ 1.f, 1.f, 1.f, 1.f };

	Matrix4f D;
	D[0] = std::array<float, 4>{ 0.f, 1.f, 0.f, 0.f };
	D[1] = std::array<float, 4>{ 0.f, 0.f, 1.f, 0.f };
	D[2] = std::array<float, 4>{ 0.f, 0.f, 0.f, 1.f };
	D[3] = std::array<float, 4>{ 1.f, 1.f, 1.f, 1.f };

	Matrix4f M{ D * S.inv() };
	Matrix4f M_inv{ M.inv() };

	//Vec4f mapped_A{ M * Vec4f{ A, 1.f } };
	Vec4f mapped_B{ M * Vec4f{ B, 1.f } };
	Vec4f mapped_C{ M * Vec4f{ C, 1.f } };

	// Mapped_A == 0, so...
	Vec3f x_vec{ Vec3f::FromVec4(mapped_B) / (float)(nb_division) };
	Vec3f y_vec{ Vec3f::FromVec4(mapped_C) / (float)(nb_division) };

	for (unsigned iy = 0; iy < nb_division; ++iy) {
		for (unsigned ix = 0; ix < nb_division; ++ix) {
			Vec3f top_left{ Vec3f::FromVec4(M_inv * Vec4f{ float(iy) * y_vec + float(ix) * x_vec, 1.f }) };
			Vec3f top_right{ Vec3f::FromVec4(M_inv * Vec4f{ float(iy) * y_vec + float(ix + 1) * x_vec, 1.f }) };
			Vec3f bot_left{ Vec3f::FromVec4(M_inv * Vec4f{ float(iy + 1) * y_vec + float(ix) * x_vec, 1.f }) };
			Vec3f bot_right{ Vec3f::FromVec4(M_inv * Vec4f{ float(iy + 1) * y_vec + float(ix + 1) * x_vec, 1.f }) };

			model->add_face(std::vector<Vec3f>{ top_left, top_right, bot_right });
			model->add_face(std::vector<Vec3f>{ top_left, bot_left, bot_right });
		}
	}

	return model;
}

std::unique_ptr<Model> create_sphere(const Vec3f& center, float radius, unsigned nb_division) {
	std::unique_ptr<Model> model{ std::make_unique<Model>() };

	constexpr float pi = 3.14159265358979323846f;
	for (unsigned dy = 0; dy < nb_division; ++dy) {
		for (unsigned dx = 0; dx < nb_division; ++dx) {
			Vec3f top_left{
				center.x + radius * std::sin(pi * dx / nb_division) * std::cos(2 * pi * dy / nb_division),
				center.y + radius * std::cos(pi * dx / nb_division),
				center.z + radius * std::sin(pi * dx / nb_division) * std::sin(2 * pi * dy / nb_division)
				
			};
			Vec3f top_right{
				center.x + radius * std::sin(pi * (dx + 1) / nb_division) * std::cos(2 * pi * dy / nb_division),
				center.y + radius * std::cos(pi * (dx + 1) / nb_division),
				center.z + radius * std::sin(pi * (dx + 1) / nb_division) * std::sin(2 * pi * dy / nb_division)
				
			};
			Vec3f bot_left{
				center.x + radius * std::sin(pi * dx / nb_division) * std::cos(2 * pi * (dy + 1) / nb_division),
				center.y + radius * std::cos(pi * dx / nb_division),
				center.z + radius * std::sin(pi * dx / nb_division) * std::sin(2 * pi * (dy + 1) / nb_division)
				
			};
			Vec3f bot_right{
				center.x + radius * std::sin(pi * (dx + 1) / nb_division) * std::cos(2 * pi * (dy + 1) / nb_division),
				center.y + radius * std::cos(pi * (dx + 1) / nb_division),
				center.z + radius * std::sin(pi * (dx + 1) / nb_division) * std::sin(2 * pi * (dy + 1) / nb_division)
				
			};

			model->add_face(std::vector<Vec3f>{ top_left, top_right, bot_right });
			model->add_face(std::vector<Vec3f>{ top_left, bot_left, bot_right });
		}
	}

	return model;
}

