// *****************************************************************************
// STD Renderer

// Authors: Maxime Rambosson
// Contact: Maxime.Rambosson@etu.unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#include <catch2/catch.hpp>

#include <Geometry.hpp>

TEST_CASE("Matrix4", "[Mat4]") {
	SECTION("Identity") {
		Matrix4f identity_1{ Matrix4f::Identity() };
		Matrix4f identity_2{ 1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f };

		REQUIRE(identity_1 == identity_2);

		REQUIRE((identity_1 * identity_2) == identity_1);

		Matrix4f matrix_1{ 2.f, 3.f, 1.f, 4.f, 7.f, 11.f, 2.f, -1.f, 5.f, 7.f, -11.f, 2.f, 3.f, 4.f, 5.f, 6.f };

		REQUIRE((identity_1 * matrix_1) == matrix_1);
		REQUIRE((matrix_1 * identity_1) == matrix_1);
	}

	SECTION("Multiplication") {
		Matrix4f identity_1{ Matrix4f::Identity() };

		REQUIRE((identity_1 * 1.f) == identity_1);
		REQUIRE((1.f * identity_1) == identity_1);

		Vec4f vec_1{ 1.f, 2.f, 3.f, 4.f };
		REQUIRE((identity_1 * vec_1) == vec_1);

		Matrix4f matrix_1{ 2.f, 3.f, 1.f, 4.f, 7.f, 11.f, 2.f, -1.f, 5.f, 7.f, -11.f, 2.f, 3.f, 4.f, 5.f, 6.f };
		REQUIRE((matrix_1 * vec_1) == Vec4f{ 27.f, 31.f, -6.f, 50.f });
	}

	SECTION("Properties") {
		Matrix4f matrix_1{ 2.f, 3.f, 1.f, 4.f,
						7.f, 11.f, 2.f, -1.f,
						5.f, 7.f, -11.f, 2.f,
						3.f, 4.f, 5.f, 6.f };

		REQUIRE(Matrix4f::Identity().det() == Approx(1.f));
		REQUIRE(matrix_1.det() == Approx(271.0f));

		REQUIRE(Matrix4f::Identity().adj() == Matrix4f::Identity());
		REQUIRE(matrix_1.adj() == Matrix4f{ -983.f, -54.f, 169.f, 590.f, 638.f, 56.f, -105.f, -381.f, -25.f, 8.f, -15.f, 23.f, 87.f, -17.f, -2.f, -15.f });

		REQUIRE(Matrix4f::Identity().inv() == Matrix4f::Identity());

		Matrix4f matrix_1_inv{ matrix_1.inv() };
		REQUIRE(matrix_1_inv[0][0] == Approx(-983.f / 271.f));
		REQUIRE(matrix_1_inv[1][2] == Approx(-105.f / 271.f));
	}

	SECTION("Transformation") {
		Matrix4f matrix_1{ 2.f, 3.f, 1.f, 4.f, 7.f, 11.f, 2.f, -1.f, 5.f, 7.f, -11.f, 2.f, 3.f, 4.f, 5.f, 6.f };
		Matrix4f identity{ Matrix4f::Identity() };

		REQUIRE(identity.toMatrix3(0, 0) == Matrix3f::Identity());
		REQUIRE(identity.toMatrix3(3, 3) == Matrix3f::Identity());
		REQUIRE(identity.toMatrix3(2, 3) == Matrix3f{ 1.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f });

		REQUIRE(matrix_1.toMatrix3(0, 0) == Matrix3f{11.f, 2.f, -1.f, 7.f, -11.f, 2.f, 4.f, 5.f, 6.f });
	}
}