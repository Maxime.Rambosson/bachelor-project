set_project("L3_Renderer")
add_rules("mode.debug", "mode.release")

add_includedirs("include")
add_headerfiles("include/**.hpp", "include/**.inl")
add_files("src/**.cpp")
set_languages("cxx17")
set_warnings("allextra")

add_requires("catch2")

target("dummy_render")
	add_files("exemples/dummy_render/**.cpp")
	add_includedirs("exemples/dummy_render")
	add_headerfiles("exemples/dummy_render/**.hpp", "exemples/dummy_render/**.inl")

target("selfcontained_cavity_render")
	add_files("exemples/selfcontained_cavity_render/**.cpp")
	add_includedirs("exemples/selfcontained_cavity_render")
	add_headerfiles("exemples/selfcontained_cavity_render/**.hpp", "exemples/selfcontained_cavity_render/**.inl")

target("overlap")
	add_files("exemples/overlap/**.cpp")
	add_includedirs("exemples/overlap")
	add_headerfiles("exemples/overlap/**.hpp", "exemples/overlap/**.inl")

target("model")
	add_files("exemples/model/**.cpp")
	add_includedirs("exemples/model")
	add_headerfiles("exemples/model/**.hpp", "exemples/model/**.inl")

target("shape")
	add_files("exemples/shape/**.cpp")
	add_includedirs("exemples/shape")
	add_headerfiles("exemples/shape/**.hpp", "exemples/shape/**.inl")

target("test")
	add_files("tests/**.cpp")
	add_includedirs("tests/shader")
	add_headerfiles("exemples/shape/**.hpp", "exemples/shape/**.inl")
	add_packages("catch2")